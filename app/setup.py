import os
from setuptools import setup, find_packages

project_dir = os.path.dirname(os.path.realpath(__file__))
requirement_file_path = project_dir + '/requirements.txt'
requirements = []
if os.path.isfile(requirement_file_path):
    with open(requirement_file_path) as f:
        requirements = f.read().splitlines()

setup(
    name='lly-snake-basket',
    version='0.0.1-dev1',
    author='Liam Lyons',
    author_email='liam.lyons@telecomnancy.eu',
    url='https://bitbucket.org/LyonsLiam/lly-snake-basket/',
    description='A sample project.',
    long_description=open('README.rst').read(),

    packages=find_packages(exclude=['tests', 'data']),
    include_package_data=True,
    install_requires=requirements,

    classifiers=[
        'Programming Language :: Python',
        'Development Status :: 1 - Planning',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.8.5',
        'Intended Audience :: Education'
    ]
)
