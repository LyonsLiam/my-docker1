class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def print_information(self):
        print("Your name is " + self.name + " and you're " + self.age + " years old.")